package com.example.mystery.mysterygame;

import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.xml.transform.Result;

/**
 * Created by HMJ on 12/25/2014.
 */
public class DirectionClass {
    List <LatLng> finalPoints = null;
    public List<LatLng> executingRequest(LatLng userLoc, LatLng destLoc, String api_k)
    {
        String url = buildDirectionURL(userLoc, destLoc, api_k);

            DownloadTask dlTask = new DownloadTask();
            dlTask.execute(url);
        try {
            String data = dlTask.get();
            Log.d("Downloaded Task:",data);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            //Log.d("OK:", "Everything is fine until Post execution of Download Task... Calling ParserTask.execute()");
            List<LatLng> returned = null;
            parserTask.execute(data);

            try {
                finalPoints = parserTask.get();
                Log.d("Final Points:",finalPoints.toString());
            }
            catch (InterruptedException e) {
                e.printStackTrace();
             }
            catch (ExecutionException e) {
                 e.printStackTrace();
             }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        return finalPoints;

    }


    public String buildDirectionURL(LatLng userLocation, LatLng destination, String api_key)
    {
        //Request URL should look like :
        //https://maps.googleapis.com/maps/api/directions/outputType?origin=Lat,Long&destination=Lat,Long&key=API_KEY
        //making Origin part of the URL
        String str_origin = "origin=" + userLocation.latitude + "," + userLocation.longitude;
        //Making Destination Part of the URL
        String str_destination = "destination=" + destination.latitude + "," + destination.longitude;

        //defining output type
        String outputType = "json";

        //binding Parameters
        String parameters = str_origin + "&" + str_destination + "&" + "key=" + api_key;

        //building final url
        String direction_url = "https://maps.googleapis.com/maps/api/directions/" + outputType +"?" + parameters;
        Log.d("Request URL:",direction_url);
        return direction_url;
    }
    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

    }
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    private class ParserTask extends AsyncTask<String, Integer, List<LatLng> > {

        // Parsing the data in non-ui thread
        @Override
        protected List<LatLng> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<LatLng> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                JSONParser parser = new JSONParser();

                // Starts parsing data
               routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }


    }

}
