package com.example.mystery.mysterygame;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;


public class FinalGame extends ActionBarActivity {
    private Button confirm;
    private EditText numberField;
    private TextView warning;
    Button btnClosePopup;
    PopupWindow pwindo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_game);

        confirm = (Button) findViewById(R.id.confirmBtn);
        numberField = (EditText)findViewById(R.id.editText);
        warning = (TextView)findViewById(R.id.warning);
        warning.setVisibility(View.INVISIBLE);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warning.setVisibility(View.INVISIBLE);
                evaluateNumber();
            }
        });


    }
    public void evaluateNumber()
    {
        if(numberField.getText().toString().trim().length() > 0) {
            int entered = Integer.parseInt(numberField.getText().toString());
            if (entered == 652) {
                Log.d("Congrats...", "You won");
                initiatePopupWindow(1);
            } else {
                Log.d("Code is not Correct...", "Try Again");
                warning.animate();
                warning.setVisibility(View.VISIBLE);
            }
        }

    }
    private void initiatePopupWindow(int invId) {
        try {
            LayoutInflater inflater = (LayoutInflater) FinalGame.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 500, 500, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            ImageView img = (ImageView)layout.findViewById(R.id.imageView);

            switch (invId)
            {
                case 1:
                {
                    img.setImageResource(R.drawable.trophy);
                    break;
                }

            }

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
            btnClosePopup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_final_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
