package com.example.mystery.mysterygame;

import android.app.Activity;
import android.app.Application;
import android.graphics.Color;
import android.graphics.Path;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity  {
    private TextView latitude;
    private TextView longitude;
    private Button requestBtn;
    private String provider;
    private LocationManager locationManager;
    private MyLocationListener mylistener;
    private Criteria criteria;
    private float lat = 0;
    private  float lon = 0;
    private  LatLng coordinations = new LatLng (0,0) ;
    private Polyline mapPolyLine;
    private Marker currentLocMarker;
    private static LatLng gare = new LatLng(49.60008,6.13374);
    private static LatLng kirchberg = new LatLng(49.6266938,6.1592935);
    private static LatLng hamilius = new LatLng(49.609300,6.12940059);
    private LatLng currentDestinationPoint = null;
    private static int distanceThreshold = 50;
    private storageClass storage = storageClass.getInstance();
    private ImageButton inventory1;
    private ImageButton inventory2;
    private ImageButton inventory3;
    private ImageButton puzzle;
    Button btnClosePopup;
    private Button mockButton;

    //pop up window set
    PopupWindow pwindo;


    SharedPreferences myPref;
    GoogleMap googleMap;

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        // Get the Camera instance as the activity achieves full user focus
       Log.d("Resumed....","Activity Resumes....");
       currentDestinationPoint = getCurrentDestination();
       unlockInventory();

    }
    public void mocking() {
        try {
            popGamePage();
        } catch (Exception e) {
            Log.d("Something went wrong Game 1", "Setting storage class level");

        }
    }
    private void initiatePopupWindow(int invId) {
        try {
            LayoutInflater inflater = (LayoutInflater) MainActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 500, 500, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            ImageView img = (ImageView)layout.findViewById(R.id.imageView);

            switch (invId)
            {
                case 1:
                {
                    img.setImageResource(R.drawable.inventory1);
                    break;
                }
                case 2:
                {
                    img.setImageResource(R.drawable.inventory2);
                    break;
                }
                case 3:
                {
                    img.setImageResource(R.drawable.inventory3);
                    break;
                }
            }

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
            btnClosePopup.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latitude = (TextView) findViewById(R.id.lat);
        longitude = (TextView) findViewById(R.id.lon);
        requestBtn = (Button) findViewById(R.id.button);
        inventory1 = (ImageButton)findViewById(R.id.inventory1);
        inventory2 = (ImageButton)findViewById(R.id.inventory2);
        inventory3 = (ImageButton)findViewById(R.id.inventory3);
        puzzle = (ImageButton) findViewById(R.id.puzzle);
        mockButton = (Button)findViewById(R.id.mockBtn);
        puzzle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent finalGame = new Intent(getApplicationContext(), FinalGame.class);
                    startActivity(finalGame);

                }catch (Exception e)
                {
                    Log.d("MainActivity:","Couldn't Start Game1 because of: "+e.toString());
                }
            }
        });
        mockButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mocking();
            }
        });

        requestBtn.setOnClickListener(new OnClickListener() {
     
        	
        	@Override
            public void onClick(View v) {
                if (googleMap != null){
                //Log.d("Clicked:", "Button");
                   if (mapPolyLine != null) {
                       deletePolyLine();
                   }
                DirectionClass direction = new DirectionClass();
                currentDestinationPoint = getCurrentDestination();
                //requestPath req = new requestPath();
                //req.execute();
                List<LatLng> returnedPnt = null;
                    if (currentDestinationPoint != null) {
                        returnedPnt = direction.executingRequest(coordinations, currentDestinationPoint, "AIzaSyAs_B5cpevNwMn4k0R0QvbT2d8Yu88SN9s");
                        Log.d("Returned Points to Main:", returnedPnt.toString());
                        drawPolyLines(returnedPnt);
                    }

            }
            }
        });

        // provText = (TextView) findViewById(R.id.prov);

        //Create Location Manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);    //fine accuracy
        criteria.setCostAllowed(false);
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        mylistener = new MyLocationListener();

        if (location != null) {
            mylistener.onLocationChanged(location);
        } else {
            // leads to the settings because there is no last known location
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        locationManager.requestLocationUpdates(provider, 1000, 1, mylistener);
        try{
            if (googleMap == null) {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            }
            googleMap.setMapType(googleMap.MAP_TYPE_NORMAL);
            if (currentLocMarker == null)
            {
                Log.d("Marker still empty","hmj");
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinations,14));
            currentLocMarker = googleMap.addMarker(new MarkerOptions().position(coordinations).title("You!"));
            //getDirection();
            googleMap.getUiSettings().setMapToolbarEnabled(false);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        currentDestinationPoint = getCurrentDestination();

       /* inventory1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inventory1.isClickable()) {
                    initiatePopupWindow(1);
                }
            }
        });
        inventory2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inventory2.isClickable()) {
                    initiatePopupWindow(2);
                }
            }
        });
        inventory3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inventory3.isClickable()) {
                    initiatePopupWindow(3);
                }
            }
        });*/

        unlockInventory();
    }



    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            // Initialize the location fields
            if (googleMap == null) {
                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            }
            latitude.setText("Latitude: "+String.valueOf(location.getLatitude()));
            longitude.setText("Longitude: "+String.valueOf(location.getLongitude()));
            //provText.setText(provider + " provider has been selected.");

            Toast.makeText(MainActivity.this,  "Location changed!",
                    Toast.LENGTH_SHORT).show();
            Log.d("Lat"+String.valueOf(location.getLatitude()),"lat");
            Log.d("long"+String.valueOf(location.getLongitude()),"long");
            lat = (float)location.getLatitude();
            lon = (float)location.getLongitude();
            coordinations = new  LatLng (lat,lon);

            if ((currentLocMarker != null)&&(coordinations != null)&&(currentDestinationPoint != null))
            {
                Log.d("NOT NULL","hmj");
                currentLocMarker.setPosition(coordinations);
                // check if current destination point is not null then proceed to check for distance difference
                if (isInRange(coordinations,currentDestinationPoint)) {
                    // user location is in range of destination point...
                    //for now is just log
                    Log.d("User In Location...","Time To Launch Game...");
                    popGamePage();
                }

            }

            //Marker currentLoc = googleMap.addMarker(new MarkerOptions().position(coordinations).title("You!"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinations,14));

            ArrayList<LatLng> locs = new ArrayList<LatLng>();
            locs.add(coordinations);
            locs.add(gare);
            Log.d("Locs:",locs.toString());
            //drawPolyLines(locs);


        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Toast.makeText(MainActivity.this, provider + "'s status changed to "+status +"!",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(MainActivity.this, "Provider " + provider + " enabled!",
                    Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(MainActivity.this, "Provider " + provider + " disabled!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void drawPolyLines(List<LatLng>listLoc)
    {
        if ( googleMap == null )
        {
            return;
        }

        if ( listLoc.size() < 2 )
        {
            return;
        }

        PolylineOptions options = new PolylineOptions();

        options.color( Color.parseColor("#CC0000FF") );
        options.width( 5 );
        options.visible( true );

        for ( LatLng locRecorded : listLoc )
        {
            options.add( locRecorded );
        }

       mapPolyLine = googleMap.addPolyline( options );

    }
    private void deletePolyLine()
    {

        mapPolyLine.remove();

    }
    private Boolean isInRange (LatLng current, LatLng dest)
    {
        Location currentLoc = new Location("Current");
        Location destinLoc = new Location("Destination");
        currentLoc.setLatitude(current.latitude);
        currentLoc.setLongitude(current.longitude);

        destinLoc.setLatitude(dest.latitude);
        destinLoc.setLongitude(dest.longitude);

        double distance = currentLoc.distanceTo(destinLoc);
        Log.d("Distance IS:",String.valueOf(distance));

        if (distance <= distanceThreshold)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    private LatLng getCurrentDestination()
    {
        LatLng currentDestination = null;
        int currentLevel = 0;
        try
        {
            currentLevel = storage.getCurrentGameLevel(this);
        }
        catch(Exception e)
        {
            Log.d("Something went wrong with Storage class:",e.toString());
        }
        switch (currentLevel)
        {
            case 1:
            {
                //kirchberg
                currentDestination = kirchberg;
                break;
            }
            case 2:
            {
                // hamilius
                currentDestination = hamilius;
                break;
            }
            case 3:
            {
                //gare
                currentDestination = gare;
                break;
            }
            default:
            {
                break;
            }
        }
        return currentDestination;
    }
    private void popGamePage()
    {

        int currentLevel = storage.getCurrentGameLevel(this);
        switch (currentLevel)
        {
            case 1:
            {
                try {
                    Intent game1 = new Intent(getApplicationContext(), MainGame1Activity.class);
                    startActivity(game1);
                    break;
                }catch (Exception e)
                {
                    Log.d("MainActivity:","Couldn't Start Game1 because of: "+e.toString());
                }
                break;
            }
            case 2:
            {
                try {
                    Intent game2 = new Intent(getApplicationContext(), MainGame2Activity.class);
                    startActivity(game2);
                    break;
                }catch (Exception e)
                {
                    Log.d("MainActivity:","Couldn't Start Game2 because of: "+e.toString());
                }
                break;
            }
            case 3:
            {
                try {
                    Intent game3 = new Intent(getApplicationContext(), MainGame3Activity.class);
                    startActivity(game3);
                    break;
                }catch (Exception e)
                {
                    Log.d("MainActivity:","Couldn't Start Game3 because of: "+e.toString());
                }
                break;
            }
            case 4:
            {
                Log.d("CASE 4:","Start FINAL MYSTERY");
                break;
            }
        }
    }
    
    //Trial SRG
    
  /*  public class Popup extends Activity implements OnClickListener { 
    	LinearLayout layoutOfPopup; 
    	PopupWindow popupMessage; 
    	TextView popupText; 
    	ImageView popupImg;
    	
    	@Override 
    	
    	public void onCreate(Bundle savedInstanceState) { 
    		super.onCreate(savedInstanceState); 
    		setContentView(R.layout.main); 
    		init(); 
    		popupInit(); 
    		} 
    	
    	public void init() { 
    		popupImg =  new ImgView(this);
    		popupText = new TextView(this); 
    		layoutOfPopup = new LinearLayout(this); 
    		popupText.setText("Game # 2");
    		ImageView iv = (ImageView)findViewById(R.id.img_selected_image);
    		String path = getApplication().getFilesDir().getAbsolutePath();
    		InputStream is = new FileInputStream(path + "/sample");
    		Drawable icon = new BitmapDrawable(is);
    		iv.setImageDrawable(icon);popupImg.setImg("");
    		popupText.setPadding(0, 0, 0, 20); 
    		layoutOfPopup.setOrientation(1); 
    		layoutOfPopup.addView(popupText); 
    		} 
    	
    	public void popupInit() { 
    		popupMessage = new PopupWindow(layoutOfPopup, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT); 
    		popupMessage.setContentView(layoutOfPopup); 
    		} 
    	
    	 }
    
    //    TRAIL SRG
    */
    
    private void unlockInventory()
    {
        // the idea is to get the current level and based on that unlock the inventories
        int currentLevel = storage.getCurrentGameLevel(this);
        switch (currentLevel)
        {
            case 1:
            {
                break;
            }
            case 2:
            {
                //unlock inventory 1--- make first button clickable and change its picture to check mark
                inventory1.setClickable(true);
                inventory1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (inventory1.isClickable()) {
                        initiatePopupWindow(1);
                    }
                }
            });

                inventory1.setImageResource(R.drawable.check);
                break;
            }
            case 3:
            {
                inventory1.setClickable(true);
                inventory2.setClickable(true);
                inventory2.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (inventory2.isClickable()) {
                            initiatePopupWindow(2);
                        }
                    }
                });
                inventory1.setImageResource(R.drawable.check);
                inventory2.setImageResource(R.drawable.check);
                break;
            }
            case 4:
            {
                inventory1.setClickable(true);
                inventory2.setClickable(true);
                inventory3.setClickable(true);
                inventory3.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (inventory3.isClickable()) {
                            initiatePopupWindow(3);
                        }
                    }
                });
                puzzle.setClickable(true);
                puzzle.setVisibility(View.VISIBLE);
                inventory1.setImageResource(R.drawable.check);
                inventory2.setImageResource(R.drawable.check);
                inventory3.setImageResource(R.drawable.check);
                break;
            }
            default:
            {
                break;
            }

        }

    }
}
