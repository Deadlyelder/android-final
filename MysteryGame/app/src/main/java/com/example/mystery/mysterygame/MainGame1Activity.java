package com.example.mystery.mysterygame;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainGame1Activity extends ActionBarActivity {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private shakeDetect mShakeDetector;
    private TextView textCounter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game1);
        textCounter = (TextView)findViewById(R.id.counter);

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new shakeDetect();
        mShakeDetector.setOnShakeListener(new shakeDetect.OnShakeListener() {

            @Override
            public void onShake(int count) {

                handleShakeEvent(count);

            }
        });


    }
    private void handleShakeEvent(int count)
    {
        Log.d("Count Shake:","Shake"+String.valueOf(count));
        String text = String.valueOf(count);
        textCounter.setText(text);
        if (count == 5)
        {
            Log.d("Reached...","Finish game...");
            try {
                storageClass storage = storageClass.getInstance();
                int currentLvl = storage.getCurrentGameLevel(getApplicationContext());
                storage.unlockNextLevel(currentLvl);
                finish();
            }catch (Exception e)
            {
                Log.d("Something went wrong Game 1","Setting storage class level");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_game1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }

}
