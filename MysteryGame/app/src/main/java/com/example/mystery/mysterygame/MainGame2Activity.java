package com.example.mystery.mysterygame;

import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Button;





        public class MainGame2Activity extends Activity implements SensorEventListener {

            private Button completeGameBtn;
            private ImageButton cocaBtn;
            private ImageButton pepsiBtn;
            private ImageButton fantaBtn;
            private ImageButton sevenUpBtn;

            private TextView warningText;
            private TextView tv;
            private SensorManager mSensorManager;
            private Sensor mGyroSensor;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            setContentView(R.layout.activity_main_game2);
           // completeGameBtn = (Button)findViewById(R.id.btnComplete);
            cocaBtn = (ImageButton)findViewById(R.id.coca);
            pepsiBtn = (ImageButton)findViewById(R.id.pepsi);
            fantaBtn = (ImageButton)findViewById(R.id.fanta);
            sevenUpBtn = (ImageButton)findViewById(R.id.sevenUp);


            cocaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Level Completed...", "Changing Level to 3");
                    try {
                        storageClass storage = storageClass.getInstance();
                        int currentLvl = storage.getCurrentGameLevel(getApplicationContext());
                        storage.unlockNextLevel(currentLvl);
                        finish();
                    }catch (Exception e)
                    {
                        Log.d("Something went wrong Game 2","Setting storage class level");
                    }
                }
            });
            pepsiBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Wrong Answer...!","Wrong...");
                    try {

                        warningText.setVisibility(View.VISIBLE);

                    }catch (Exception e)
                    {
                        Log.d("Something went wrong Game 2","Setting storage class level");
                    }
                }
            });
            fantaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Wrong Answer...!","Wrong...");
                    try {
                        warningText.setVisibility(View.VISIBLE);

                    }catch (Exception e)
                    {
                        Log.d("Something went wrong Game 2","Setting storage class level");
                    }
                }
            });
            sevenUpBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Wrong Answer...!","Wrong...");
                    try {
                        warningText.setVisibility(View.VISIBLE);

                    }catch (Exception e)
                    {
                        Log.d("Something went wrong Game 2","Setting storage class level");
                    }
                }
            });

            warningText = (TextView)findViewById(R.id.warningMsg);
            //tv= (TextView)findViewById(R.id.text2);
            warningText.setVisibility(View.INVISIBLE);

            // Get an instance of the sensor service
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            mGyroSensor=mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

            PackageManager PM= this.getPackageManager();
            boolean gyro = PM.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
            boolean light = PM.hasSystemFeature(PackageManager.FEATURE_SENSOR_LIGHT);

           /* if(gyro){


               // Toast.makeText(getApplicationContext(),"Both light and gyroscope sensors are present", Toast.LENGTH_LONG).show();
            }
            else{}
              //  Toast.makeText(getApplicationContext(),"Only gyroscope sensor is present", Toast.LENGTH_LONG).show();

*/
        }
        @Override
        public final void onAccuracyChanged(Sensor sensor, int accuracy) {
            // Some popup when it changes?
        }

        @Override
        public final void onSensorChanged(SensorEvent event) {

            float angularXSpeed = event.values[0];
           // tv.setText("Angular X speed level is: " + "" +angularXSpeed);
        }

        @Override
        protected void onResume() {
            // Register a listener for the sensor.
            super.onResume();
            mSensorManager.registerListener(this, mGyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        protected void onPause() {
            // Deregister sensor
            super.onPause();
            mSensorManager.unregisterListener(this);
        }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_game2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

