package com.example.mystery.mysterygame;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;


public class MainGame3Activity extends ActionBarActivity {
    private Button completeGameBtn;
    private EditText A_Label;
    private EditText B_Label;
    private TextView warning;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game3);
        completeGameBtn = (Button)findViewById(R.id.btnComplete);
        A_Label = (EditText)findViewById(R.id.editA);
        B_Label = (EditText)findViewById(R.id.editB);
        warning = (TextView)findViewById(R.id.warningMSG);
        completeGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Level Completed...", "Changing Level to 4");
                try {
                    evaluateNumber();
                }catch (Exception e)
                {
                    Log.d("Something went wrong Game 3","Setting storage class level");
                }
            }
        });

    }
    public void evaluateNumber()
    {
        if(A_Label.getText().toString().trim().length() > 0 && B_Label.getText().toString().trim().length()>0) {
            int entered_A = Integer.parseInt(A_Label.getText().toString());
            int entered_B = Integer.parseInt(B_Label.getText().toString());
            if ((entered_A == 7)&&(entered_B==5)) {
                Log.d("Congrats...", "You won");
                //change page
                storageClass storage = storageClass.getInstance();
                int currentLvl = storage.getCurrentGameLevel(getApplicationContext());
                storage.unlockNextLevel(currentLvl);
                finish();
            } else {
                Log.d("Code is not Correct...", "Try Again");
                warning.animate();
                warning.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_game3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
