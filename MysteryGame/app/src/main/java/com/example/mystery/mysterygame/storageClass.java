package com.example.mystery.mysterygame;

import android.content.Context;
import android.content.SharedPreferences;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import java.security.PublicKey;

/**
 * Created by HMJ on 12/27/2014.
 */
public class storageClass {
    private SharedPreferences appSharedPrefs;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Level = "nameKey";
    public static final String Phone = "phoneKey";
    public int currentLevel=0;
    private static storageClass ourInstance = new storageClass();
    public static storageClass getInstance() {
        return ourInstance;
    }

    private storageClass() {

    }
    public int getCurrentGameLevel (Context context)
    {
        //Created appshared prefrences to store data
        this.appSharedPrefs = context.getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);

        //Check if the preferences is empty create one with default value of one
        if (!(appSharedPrefs.contains(Level)))
        {
            //Create app pref with key Level and Value of 1
            Editor editor = appSharedPrefs.edit();
            editor.putInt(Level,1);
            editor.commit();
        }
        // It will get current level, if the key isn't there it will give 1 as default
        currentLevel = appSharedPrefs.getInt(Level, 1);
        Log.d("Storage Level:",String.valueOf(currentLevel));
        return currentLevel;
    }
    public void unlockNextLevel (int i)
    {
        int currentLvl = i;

        switch (currentLvl){
            case 1:{
                //change to Level 2
                writeLevelToPref(2);
                break;
            }
            case 2:{
                //Change to Level 3
                writeLevelToPref(3);
                break;
            }
            case 3:{
                //change to level 4
                writeLevelToPref(4);
                break;
            }
        }
    }
    private void writeLevelToPref (int i)
    {
        Editor editor = appSharedPrefs.edit();
        editor.putInt(Level,i);
        editor.commit();
        Log.d("Storage LVL CHANGE:",String.valueOf(i));
    }
}
